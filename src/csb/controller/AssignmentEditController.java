package csb.controller;

import csb.data.Assignment;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.AssignmentDialog;
import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import javafx.collections.ObservableList;
import static csb.CSB_PropertyType.REMOVE_ASSIGNMENT_MESSAGE;
/**
 *
 * @author Kimberly Allan, adapted from ScheduleEditController by McKenna
 */
public class AssignmentEditController {
    AssignmentDialog ad;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public AssignmentEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ad = new AssignmentDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    public void handleAddAssignmentRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ad.showAddAssignmentDialog(course.getStartingMonday());

        if (ad.wasCompleteSelected()) {
            Assignment hw = ad.getAssignment();           
            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(hw);
            //dirty
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }

    public void handleEditAssignmentRequest(CSB_GUI gui, Assignment hwToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        ad.showEditAssignmentDialog(hwToEdit);
        
        // DID THE USER CONFIRM?
        if (ad.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Assignment hw = ad.getAssignment();
            hwToEdit.setName(hw.getName());
            hwToEdit.setTopics(hw.getTopics());
            hwToEdit.setDate(hw.getDate());
            //dirty
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveAssignmentRequest(CSB_GUI gui, Assignment hwToRemove) {
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ASSIGNMENT_MESSAGE));
        
        String selection = yesNoCancelDialog.getSelection();

        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeAssignment(hwToRemove);
            //dirty
            gui.getFileController().markAsEdited(gui);
        }
    }

    
}
