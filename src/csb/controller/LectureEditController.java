package csb.controller;

import static csb.CSB_PropertyType.REMOVE_LECTURE_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.LectureDialog;
import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import javafx.collections.ObservableList;
/**
 *
 * @author Kimberly Allan, adapted from ScheduleEditController by McKenna
 */
public class LectureEditController {
    LectureDialog ld;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ld = new LectureDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    public void handleAddLectureRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ld.showAddLectureDialog();

        if (ld.wasCompleteSelected()) {
            Lecture lec = ld.getLecture();           
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(lec);
            //dirty
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }

    public void handleEditLectureRequest(CSB_GUI gui, Lecture lectureToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        ld.showEditLectureDialog(lectureToEdit);
        
        // DID THE USER CONFIRM?
        if (ld.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Lecture lec = ld.getLecture();
            lectureToEdit.setTopic(lec.getTopic());
            lectureToEdit.setSessions(lec.getSessions());
            //dirty
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveLectureRequest(CSB_GUI gui, Lecture lectureToRemove) {
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_LECTURE_MESSAGE));
        
        String selection = yesNoCancelDialog.getSelection();

        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(lectureToRemove);
            //dirty
            gui.getFileController().markAsEdited(gui);
        }
    }
    /**
     * 
     * @param lectureToMove
     * @param tableList 
     * @return row moved or negative if nothing moved
     */
    public int handleUpLectureRequest(CSB_GUI gui, Lecture lectureToMove, ObservableList<Lecture> tableList) {
        //Can't move anything
        if (tableList.size() <= 1)
            return -1;
        int i = tableList.indexOf(lectureToMove);
        // if lecture as others listed above
        if (i > 0) {
            Lecture temp = (Lecture) tableList.get(i);
            tableList.set(i, tableList.get(i - 1));
            tableList.set(i - 1, temp);
            //dirty
            gui.getFileController().markAsEdited(gui);
            return i - 1;    
        }   
        return -1;
    }
    /**
     * 
     * @param lectureToMove
     * @param tableList
     * @return row moved or negative if nothing moved
     */
    public int handleDownLectureRequest(CSB_GUI gui, Lecture lectureToMove, ObservableList<Lecture> tableList) {
        //Can't move anything
        if (tableList.size() <= 1)
            return -1;
        int i = tableList.indexOf(lectureToMove);
        // if lecture as others listed below
        if (i < (tableList.size() - 1)) {
            Lecture temp = (Lecture) tableList.get(i);
            tableList.set(i, tableList.get(i + 1));
            tableList.set(i + 1, temp);
            //dirty
            gui.getFileController().markAsEdited(gui);
            return i + 1;   
        } 
        return -1;
    }
    
}
