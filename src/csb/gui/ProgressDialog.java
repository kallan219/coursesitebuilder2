package csb.gui;

import csb.data.CoursePage;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Kimberly Allan, adapted from ScheduleItemDialog from McKenna
 */
public class ProgressDialog extends Stage {
    
    // GUI CONTROLS FOR OUR DIALOG
    HBox row1;
    HBox row2;
    VBox panes;
    Scene dialogScene;
    Label headingLabel;
    Label currPageLabel;
    Label descriptionLabel;
    ProgressBar bar;
    ProgressIndicator perc; 
    
    public static final String PROGRESS_HEADING = "EXPORTING ";
    public static final String PAGE_EXPORT_COMPLETE = " Completed";
    public static final String PAGE_EXPORT_PROGRESSING = " ...";
    
    public ProgressDialog(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        row1 = new HBox();
        row2 = new HBox();
        panes = new VBox();
        row1.setPadding(new Insets(10, 20, 20, 20));
        row2.setPadding(new Insets(10, 20, 20, 20));
        this.setHeight(200);
        this.setWidth(550);
        
        
        headingLabel = new Label(PROGRESS_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        currPageLabel = new Label();
        currPageLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        descriptionLabel = new Label(PAGE_EXPORT_PROGRESSING);
        descriptionLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        bar = new ProgressBar(0);
        perc = new ProgressIndicator(0);
           
        row1.getChildren().add(headingLabel);
        row1.getChildren().add(currPageLabel);
        row1.getChildren().add(descriptionLabel);
        row2.getChildren().add(bar);
        row2.getChildren().add(perc);
        panes.getChildren().add(row1);
        panes.getChildren().add(row2); 
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(panes);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public void setCurrPage(CoursePage page) {
        currPageLabel.setText(page.toString());
    }
    
    public void completed(boolean done) {
        if (done) {
            descriptionLabel.setText(PAGE_EXPORT_COMPLETE);           
        } else {
            descriptionLabel.setText(PAGE_EXPORT_PROGRESSING);
        }
    }
    
    /** Sets progress
        @param prog number between 0 and 1 (equivalent to 0 - 100 */
    public void setProgress(double prog) {
        if (prog >= 0 && prog <= 1) {
            bar.setProgress(prog);
            perc.setProgress(prog);
        } else {
            throw new IllegalArgumentException("Value must be between 0 and 1 inclusive");
        }       
    }
    
    public void showProgressDialog() {
        this.showAndWait();
    }
    
    
}
