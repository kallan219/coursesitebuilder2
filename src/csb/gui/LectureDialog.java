package csb.gui;

import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.time.temporal.ChronoUnit;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 *
 * @author Kimberly Allan, adapted from ScheduleItemDialog by McKenna
 */
public class LectureDialog extends Stage {
    // Object Data
    Lecture lecture;
    
    // GUI CONTROLS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    TextField topicTextField;
    Label sessionsLabel;
    ComboBox sessionsComboBox;
    Button completeButton;
    Button cancelButton;
    
    // BUTTON PRESSED
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TOPIC_PROMPT = "Topic: ";
    public static final String SESSIONS_PROMPT = "Number of Sessions: ";
    public static final String LECTURE_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    public static final String EDIT_LECTURE_TITLE = "Edit Lecture";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public LectureDialog(Stage primaryStage, Course course,  MessageDialog messageDialog) {       
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        headingLabel = new Label(LECTURE_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        //TOPIC
        topicLabel = new Label(TOPIC_PROMPT);
        topicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setTopic(newValue);
        });
        
        //NYMBER OF SESSIONS
        sessionsLabel = new Label(SESSIONS_PROMPT);
        sessionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        sessionsComboBox = new ComboBox();   
        //what should this value be???????????????????????????????????????????????????????????????
        int maxSes = 20;
        for (int i = 1; i <= maxSes; i++)
                sessionsComboBox.getItems().add(Integer.toString(i));     
        sessionsComboBox.setOnAction(e -> {lecture.setSessions(Integer.parseInt(sessionsComboBox.getValue().toString()));});
        
        //DIALOG CONTROLS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureDialog.this.selection = sourceButton.getText();
            LectureDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(topicLabel, 0, 1, 1, 1);
        gridPane.add(topicTextField, 1, 1, 1, 1);
        gridPane.add(sessionsLabel, 0, 2, 1, 1);
        gridPane.add(sessionsComboBox, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Lecture getLecture() { 
        return lecture;
    }
    
    public Lecture showAddLectureDialog() {
        setTitle(ADD_LECTURE_TITLE);
        
        lecture = new Lecture();
        
        topicTextField.setText(lecture.getTopic());
        sessionsComboBox.setValue(lecture.getSessions());
        
        this.showAndWait();       
        return lecture;
    }
    
    public void loadGUIData() {
        topicTextField.setText(lecture.getTopic());
        sessionsComboBox.setValue(lecture.getSessions());    
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditLectureDialog(Lecture lectureToEdit) {
        setTitle(EDIT_LECTURE_TITLE);

        lecture = new Lecture();
        lecture.setTopic(lectureToEdit.getTopic());
        lecture.setSessions(lectureToEdit.getSessions());
        
        loadGUIData();
        this.showAndWait();
    }
}
